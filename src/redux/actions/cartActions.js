import { cartTypes } from "../types/cartTypes";

export function setCart(modalType, artNum, products, cart) {
    const currentProduct = products.find(product => product.artNum === artNum)
    let newCart = []

    if (modalType === 'buy') {
        newCart = [...cart, currentProduct]
    } else if (modalType === 'delete') {
        const currentProductIndex = cart.lastIndexOf(cart.find(product => product.artNum === artNum))
        newCart = [...cart.slice(0, currentProductIndex), ...cart.slice(currentProductIndex + 1)]
    }

    localStorage.setItem('cart', JSON.stringify(newCart))
    return dispatch => dispatch(setCartAction(newCart))
}

function setCartAction(payload) {
    return {
        type: cartTypes.SET_CART,
        payload
    }
}
